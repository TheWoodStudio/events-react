import React, { Component } from 'react';
import Header from './components/Header';
import Form from './components/Form/FormBody';

class App extends Component {
  token = 'YLVPJ4WZ2SKAROJGJ3AK';

  state = {
    categories: [],
    events: []
  }

  componentDidMount() {
    this.getCategories();
  }

  getCategories = () => {
    let url = `https://www.eventbriteapi.com/v3/categories/?token=${this.token}&locale=es`;

    fetch(url)
      .then(response => {
        return response.json();
      })
      .then(data => {
        this.setState({
          categories: data.categories
        })
      })
  }

  getEvents = (search) => {
    const name = search.event;
    const category = search.category;
    let url = `https://www.eventbriteapi.com/v3/events/search/?q=${name}&sort_by=date&categories=${category}&token=${this.token}&locale=es`;
    
    fetch(url)
      .then(response => {
        return response.json();
      })
      .then(data => {
        this.setState({
          events: data.events
        })
      })
  }

  render() {
    return (
      <div className="App">
        <Header
          title="Events in your area"
        />
        <Form
          categories={this.state.categories}
          getEvents={this.getEvents}
        />
      </div>
    );
  }
}

export default App;
