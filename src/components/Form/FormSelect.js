import React, { Component } from 'react';
import Select from 'muicss/lib/react/select';
import Option from 'muicss/lib/react/option';

class FormSelect extends Component {
    getOptions = (key) => {
        const category = this.props.categories[key];
        const { id, name } = category;
        if (!id || !name) return null;
        return (
            <Option key={key} value={id} label={name}/>
        )
    }

    render() { 
        const categories = Object.keys(this.props.categories);

        return ( 
            <Select placeholder={this.props.placeholder} onChange={this.props.onChange} value={this.props.value} name={this.props.name} >
                {categories.map(this.getOptions)}
            </Select>
         );
    }
}
 
export default FormSelect;