import React, { Component } from 'react';
import styled from 'styled-components';
import FormInput from './FormInput';
import FormSelect from './FormSelect';
import FormButton from './FormButton';

const StyledForm = styled.form`
    max-width: 600px;
    margin: 0 auto;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding: 1rem 2rem;
    background-color: #ffffe5;
    box-shadow: 0 2px 2px 0 rgba(0,0,0,.16), 0 0 2px 0 rgba(0,0,0,.12);

    @media (max-width: 700px) {
        flex-direction: column;
    }
`;

class FormBody extends Component {
    state = {
        inputValue: '',
        selectValue: ''
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    searchEvents = (e) => {
        e.preventDefault();
        
        //Create object
        const searchData = {
            event: this.state.inputValue,
            category: this.state.selectValue
        }
        
        //Pass it through props
        this.props.getEvents(searchData);
    }

    render() { 
        const categories = this.props.categories;

        return ( 
            <StyledForm onSubmit={this.searchEvents}>
                <FormInput label="Event Name or City" type="text" value={this.state.inputValue} onChange={this.handleChange} name="inputValue"/>
                <FormSelect categories={categories} placeholder="Category" value={this.state.selectValue} onChange={this.handleChange} name="selectValue"/>
                <FormButton color="primary" content="search" />
            </StyledForm>
         );
    }
}
 
export default FormBody;