import React, { Component } from 'react';
import Input from 'muicss/lib/react/input';

class FormInput extends Component {
    render() { 
        return ( 
            <Input label={this.props.label} type={this.props.type} floatingLabel={true} onChange={this.props.onChange} value={this.props.value} name={this.props.name}/>
         );
    }
}
 
export default FormInput;