import React from 'react';
import Button from 'muicss/lib/react/button';

const FormButton = (props) => {
    return ( 
        <Button color={props.color}>{props.content}</Button>
     );
}
 
export default FormButton;