import React from 'react';
import styled from 'styled-components';

const StyledHeader = styled.header`
    background-color: #fff8e1;
    text-align: center;
    padding: 1rem 0;

    h1 {
        margin: 0;
    }
`;

const Header = (props) => {
    return ( 
        <StyledHeader>
            <h1>{props.title}</h1>
        </StyledHeader>
     );
}
    
export default Header;